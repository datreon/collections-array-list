package com;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ArrayListHelper a =new ArrayListHelper();
        a.createArrayList();


        Hospital h1 = new Hospital(002, "AIIMS", "South Delhi");
        Hospital h2 = new Hospital(235, "Safdarjung", "South-East Delhi");
        Hospital h3 = new Hospital(030, "Pentamed", "Model Town");

        List<Hospital> hospitalNames = new ArrayList<>();
        hospitalNames.add(h1);
        hospitalNames.add(h2);
        hospitalNames.add(h3);

        //for each example
        for (Hospital names : hospitalNames) {
            System.out.println(names);
        }

        // Iterator example
        System.out.println("All hospital : "  + hospitalNames);

        Iterator<Hospital> hIterator = hospitalNames.iterator();

        while(hIterator.hasNext()){

            Hospital hospital = hIterator.next();
            if(hospital.getId() > 10)
                System.out.println(hospital);
            else
                hIterator.remove();
        }
        System.out.println("After loop & condition : " + hospitalNames);
    }


}
