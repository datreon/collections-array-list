package com;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ArrayListHelper {
    private List<String> hospitalNames;

    public ArrayListHelper() {
        this.hospitalNames = null;
    }

    public List<String> getHospitalNames() {
        return hospitalNames;
    }

    public void setHospitalNames(List<String> hospitalNames) {
        this.hospitalNames = hospitalNames;
    }

    public void createArrayList() {
        
        List<String> hospitalNames = new ArrayList<>();

        // adding elements to list
        hospitalNames.add("BLK");
        hospitalNames.add("Max");
        hospitalNames.add("Fortis");
        hospitalNames.add("Apollo");
        hospitalNames.add("AIIMS");

        //sort arrayList
        Collections.sort(hospitalNames);

        //print list
        System.out.println("The names are : " + hospitalNames);

        //access via new for-loop
        //for each
        for (String names : hospitalNames) {
            System.out.println(names);
        }

        //get size of arrayList
        int size = hospitalNames.size();
        System.out.println("Size of list is : " + size);

        //replace element from any index
        hospitalNames.set(2, "BLK Delhi");

        System.out.println("Element at index 2 replaced : " + hospitalNames);

        // check an element in list
        System.out.println("Is BLK in the list  : " + hospitalNames.contains("BLK"));

        //check if list is empty
        System.out.println("Is list empty : " + hospitalNames.isEmpty());

        //remove an element
        hospitalNames.remove(4);
        System.out.println(hospitalNames);

//        //copying from one to another
//        ArrayList<String> copyOfStringList = new ArrayList<String>();
//        copyOfStringList.addAll(hospitalNames);
//
//        System.out.println("Copy of list : " + copyOfStringList);
//
//        Iterator<String> nameItertor = hospitalNames.iterator();
//        while(nameItertor.hasNext()){
//            System.out.println(nameItertor.next());
//        }



    }
}
